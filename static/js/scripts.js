//On Load Javascript

$(document).ready(function(){
	// Sign Up Form 
	$('.fancy').fancybox();

	// Clear Forms on Refresh
	$('input[type!="button"][type!="submit"], select, textarea')
         .val('')
         .blur();

    // Login Form Animations
    var input1 = $('#email');
	var input2 = $('#password');

	$('#email').focus(function(){
	    $('#email-label').animate({'bottom':'40px'}, 100);
	    $('#email-label').animate({'opacity':'1'}, 100);
	});
	$('#email').blur(function(){
		if (input1.val().length == 0) {
			$('#email-label').animate({'bottom':'0px'}, 100);
		} else {
	    	$('#email-label').animate({'opacity':'0'}, 100);
		}
	});
	$('#password').focus(function(){
	    $('#password-label').animate({'bottom':'40px'}, 100);
	    $('#password-label').animate({'opacity':'1'}, 100);
	});
	$('#password').blur(function(){
		if (input2.val().length == 0) {
			$('#password-label').animate({'bottom':'0px'}, 100);
		} else {
	    	$('#password-label').animate({'opacity':'0'}, 100);
	    }
	});
});
