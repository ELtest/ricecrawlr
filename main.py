#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
from webapp2_extras import auth
from webapp2_extras import sessions
import logging
import os
import jinja2
from google.appengine.ext import ndb
import urllib2
import json
import random
from random import randint
from webapp2_extras.auth import InvalidAuthIdError
from webapp2_extras.auth import InvalidPasswordError
import model
from model import Event
from model import User
from model import NameMap
from twilio.rest import TwilioRestClient 
from keys import AUTH_TOKEN, ACCOUNT_SID

jinja_environment = jinja2.Environment(loader =
    jinja2.FileSystemLoader(os.path.dirname(__file__)))

#do not touch or edit, this class works and has been checked
class BaseHandler(webapp2.RequestHandler):
    @webapp2.cached_property
    def auth(self):
        return auth.get_auth()

    @webapp2.cached_property
    def user_info(self):
        return self.auth.get_user_by_session()

    @webapp2.cached_property
    def user(self):
        u = self.user_info
        return self.user_model.get_by_id(u['user_id']) if u else None

    @webapp2.cached_property
    def user_model(self): 
        return self.auth.store.user_model

    @webapp2.cached_property
    def session(self):
        return self.session_store.get_session(backend="datastore")

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
            user = self.user_info
            params['user'] = user
            cur_template = jinja_environment.get_template(view_filename)
            self.response.out.write(cur_template.render(params))

    # this is needed for webapp2 sessions to work
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)

#do not touch or edit, this class works and has been checked
class MainHandler(BaseHandler):
    def get(self):
        # namemap = NameMap()
        # namemap.mapping = {}
        # namemap.put()
        user = self.user_info
        home_template = jinja_environment.get_template('home.html')
        home_template_values = {'user':user}#what is this being passed for? webapp2 probably
        self.response.out.write(home_template.render(home_template_values))

#do not touch or edit, this class works and has been checked
class SignupHandler(BaseHandler):
    def post(self):
        #get signup information
        signup_name = self.request.get('name')
        signup_email = self.request.get('email')
        signup_phone = self.request.get('phone')
        initial_event_list = ''
        #make netid equal to netid (Rice style)
        netid = signup_email[:-9]
        password = self.request.get('password')
        confirm = self.request.get('confirm')
        #set template to home.html why? - to refresh page in case of login failure
        home_template = jinja_environment.get_template('home.html')

        if password == confirm and signup_email[-9:] == '@rice.edu' and not signup_phone[:2] == '+1' and len(signup_phone) == 10:
            signup_phone = "+1" + signup_phone
            #list of things that must be unique when signing up
            unique_properties = ['email', 'phone']
            #first param is required auth_id, followed by extra unique things
            user_data = self.user_model.create_user(netid, unique_properties, email=signup_email, phone=signup_phone, event_list=initial_event_list, name=signup_name, password_raw=password, verified=True)
            #add name:netid to the NameMap object
            namemap = NameMap.get_by_id(5418830851473408)

            namemap.mapping[netid] = signup_name
            namemap.put()
            # user attempts to make an account that already exists (phone, email)-will refresh page ?maybe set up a notification that you tried to create an account that already exists?
            if not user_data[0]:
                home_template_values = {'login_fail':True}#only value needed for home template
                self.response.out.write(home_template.render(home_template_values))
            # successful account creation
            else:
                user = user_data[1]
                user_id = user.get_id()
                #not sure what token is? - can possibly be deleted or perhaps it will break program
                first_event_list = []
                token = self.user_model.create_signup_token(user_id)
                self.auth.set_session(self.auth.store.user_to_dict(user), remember=True)#this looks important for webapp2 so keeping
                dash_template_values= {'current_user': signup_name, 'netid':netid, 'event_list':first_event_list}#necessary for dashboard to load - only these three parameters are necessary
                dash_template = jinja_environment.get_template('dashboard.html')#go to the dashboard with an empty event list
                self.response.out.write(dash_template.render(dash_template_values))
        #passwords don't match or non-rice email
        else:
            bad_passwords = True
            home_template_values = {'bad_passwords':bad_passwords}
            self.response.out.write(home_template.render(home_template_values))#reload home login page

def go_to_dash(self, netid, current_user):
    id_as_string_list = User.get_by_auth_id(netid).event_list.split(',')#parses the string event_list into a list of the Event IDs that
    id_list = [int(x) for x in id_as_string_list if not x == '']#this netid is currently attending (which means this netid should
    event_list = [Event.get_by_id(x) for x in id_list]           #be in the attendees data structure of every Event object)
    dash_template_values = {'netid':netid, 'current_user':current_user, 'event_list':event_list}
    dash_template = jinja_environment.get_template('dashboard.html')#go to dashboard with these three values
    self.response.out.write(dash_template.render(dash_template_values))

class LoginHandler(BaseHandler):
    def get(self): #possibly unnecessary 
        self.post()
    
    def post(self):
        netid = self.request.get('netid')
        user_password = self.request.get('password')
        try:#tries to login and throws except if error returns and then reloads the home page
            user = self.auth.get_user_by_password(netid, user_password, remember=True,
                save_session=True)
            current_user = user['name']#this is becuase get_user_by_password retuns a dictionary type for User
            go_to_dash(self, netid, current_user)#calls dashboard function just like DashHandler

        except (InvalidPasswordError, InvalidAuthIdError) as e:
            #if username does not work
            home_template = jinja_environment.get_template('home.html')
            #on home.html have a message pop up saying that the username and password combination did not work
            #if login fail is True
            #We can specify if the failure was on account of email being wrong vs password being wrong if we want
            home_template_values = {'login_fail': True}
            self.response.out.write(home_template.render(home_template_values))

class DashHandler(BaseHandler):
    def get(self):
        netid = self.request.get('netid')
        current_user = User.get_by_auth_id(netid).name
        go_to_dash(self, netid, current_user)#calls dashboard function just like DashHandler

"""
Currently the items that are being passed to event_page.html are as follows
event_name
event_time
event_date
host
event_description
event_id
netid
search_done
host_name
stop_list
current_user
host_phone
event_hosts

We need to find a better way to package and organize this information before passing it
Jake will take care of this

"""
   
class CrawlEventHandler(BaseHandler):
    def get(self):
        new_crawl = self.request.get('new')
        netid = self.request.get('netid')
        if new_crawl:
            #get all info about crawl
            event_name = self.request.get('event_name')#string
            event_date = self.request.get('event_date')#date object
            event_time = self.request.get('event_time')#time object
            event_host = self.request.get('host')
            event_description = self.request.get('event_description')
            stop_list = []
            event_hosts = [event_host]
            attendees = [netid]#add the creator of event to the attendees list of the event object
            new_event = Event(name = event_name, description=event_description, date = event_date, time= event_time, host_list= event_hosts, stop_list=stop_list, attendees=attendees)#make new event object
            event_attendees = new_event.attendees
            key = new_event.put()#push this to the datastore
            crawl_id = key.id()#get crawl Event id from returned key once new_event object is put in datastoref
            crawl = Event.get_by_id(crawl_id)
            current_user = User.get_by_auth_id(netid)#get User object from netid
            current_user.event_list += str(crawl_id) + ','#add the crawl Event id to the event_list for the current user
            current_user.put()#push the changed user object back to the datastore

        else:
            crawl_id = int(self.request.get('event_id'))#get the crawlid from the url taken from event_page
            crawl = Event.get_by_id(crawl_id)
            #basic info from crawl id
            event_name = crawl.name
            event_date = crawl.date
            event_time = crawl.time
            event_host = crawl.host_list[0]

            current_user = User.get_by_auth_id(netid)

        #how to retrieve from datastore
        #query = Event.query().filter(Event.name == 'test3')
        #put this \/ in template value if you wanna see it 
        #results = query.fetch(10)
        event_hosts = crawl.host_list
        event_attendees = [User.get_by_auth_id(x) for x in crawl.attendees]

        stop_list = Event.get_by_id(crawl_id).stop_list
        host_list = Event.get_by_id(crawl_id).host_list
        host = User.get_by_auth_id(event_host)
        host_phone = host.phone
        host_name = host.name
        event_template = jinja_environment.get_template('event_page.html')
        current_user = current_user.name
        event_template_values = {'event_name': event_name, 'event_date':event_date,
        'event_time':event_time,'event_host':event_host, 'search_done':False,
         'netid':netid, 'event_id':crawl_id, 'stop_list':stop_list, 'event_attendees':event_attendees,
         'current_user':current_user, 'host_phone':host_phone, 'host_name':host_name, 'event_hosts':event_hosts}
        self.response.out.write(event_template.render(event_template_values))

#do not touch or edit, this class works and has been checked
class SignoutHandler(BaseHandler):
    def post(self):
        self.auth.unset_session()#end session for user
        home_template_values = {'login_fail':False}#you don't want INVALID LOGIN to pop up
        home_template = jinja_environment.get_template('home.html')    
        self.response.out.write(home_template.render(home_template_values))#reload the home page

class SearchPeopleHandler(BaseHandler):
    def get(self):
        search_results = self.do_search(self.request.get('user_search_name'))
        #get info from previous event page
        netid = self.request.get('netid')
        current_user = User.get_by_auth_id(netid).name

        #set event template
        event_template = jinja_environment.get_template('event_page.html')
        crawl_id = int(self.request.get('event_id'))
        crawl = Event.get_by_id(crawl_id)
        stop_list = crawl.stop_list#get stop list to pass back into event_page
        event_name = crawl.name#string
        event_date = crawl.date#date object
        event_time = crawl.time#time object
        event_host = crawl.host_list[0] # string, host's netid
        event_hosts = crawl.host_list
        search_done = True
        host = User.get_by_auth_id(event_host)
        host_phone = host.phone
        host_name = host.name
        event_attendees = [User.get_by_auth_id(x) for x in Event.get_by_id(crawl_id).attendees]#get event attendees again
        event_template_values = {'event_name': event_name, 'event_date':event_date,
        'event_time':event_time,'event_host':event_host, 'search_done':search_done,
        'netid':netid, 'event_id':crawl_id, 'stop_list':stop_list, 'search_result_list':search_results,'event_attendees':event_attendees,
        'current_user':current_user, 'host_phone':host_phone, 'host_name':host_name, 'event_hosts':event_hosts}
        self.response.out.write(event_template.render(event_template_values))

    def do_search(self, user_search_name):
        #hardcoded namemap used only for searching since User objects are not filterable by our added trait 'name'
        namemap = NameMap.get_by_id(5418830851473408)
        #used for clearing the namemap
        #namemap.mapping = {}
        #namemap.put()
        name_and_netid_list = []#going to be a list of tuples containing netids and names
        for netid, name in namemap.mapping.items():
            if user_search_name.lower() in name.lower():#if the searched user name is found (not full name required)
                name_and_netid_list.append((name, netid))#append the tuple
        return name_and_netid_list

class AddStopHandler(BaseHandler):
    def get(self):
        stop_info = {}#stop information stored as a dictionary (all info below retreived from input boxes on event_page add stop section)
        stop_info['stop_name'] = self.request.get('stop_name')
        stop_info['college_location'] = self.request.get('college_location')
        stop_info['room_number'] = self.request.get('room_number')
        host = self.request.get('stop_host')#this is the netid of the stop host
        stop_info['stop_host'] = User.get_by_auth_id(host) #stop_host is now the user object
        crawl_id = int(self.request.get('event_id'))
        netid = self.request.get('netid')
        crawl = Event.get_by_id(crawl_id)
        event_name = crawl.name#string
        event_date = crawl.date#date object
        event_time = crawl.time#time object
        current_user = User.get_by_auth_id(netid).name
        #add the stop info to the stop list entity of the Event object

        
        crawl.stop_list.append(stop_info)
        crawl.put()#push the crawl object into the datastore
        host_id = crawl.host_list[0]
        event_hosts = crawl.host_list
        event_attendees = [User.get_by_auth_id(x) for x in crawl.attendees]#get event attendees again
        host = User.get_by_auth_id(host_id)
        host_name = host.name
        host_phone = host.phone
        event_template = jinja_environment.get_template('event_page.html')
        event_template_values = {'event_name': event_name, 'event_date':event_date,
        'event_time':event_time, 'search_done':False,
        'netid':netid, 'event_id':crawl_id, 'stop_list':crawl.stop_list, 'event_attendees':event_attendees,
        'current_user':current_user, 'host_name':host_name, 'host_phone':host_phone, 'event_hosts':event_hosts}#search results list is not necessary to pass in because search_done is False, so this is all the info that needs to be passed in to event_page
        self.response.out.write(event_template.render(event_template_values))#reload page

# this is unused right now
class MoveStopHandler(BaseHandler):
    def get(self):
        netid= self.request.get('netid')
        crawl_id= int(self.request.get('event_id'))
        current_index= int(self.request.get('current_index'))
        new_index= int(self.request.get('new_index'))
        delete_index = int(self.request.get('delete_index'))
        crawl = Event.get_by_id(crawl_id)
        event_name = crawl.name#string
        event_date = crawl.date#date object
        event_time = crawl.time#time object
        current_user = User.get_by_auth_id(netid).name
        moved_stop= crawl.stop_list.pop(current_index)
        crawl.stop_list.insert(new_index, moved_stop)
        deleted_stop = crawl.stop_list.pop(delete_index)
        crawl.put()
        stop_list = crawl.stop_list
        event_host = crawl.host_list[0]
        event_hosts = crawl.host_list
        event_attendees = [User.get_by_auth_id(x) for x in crawl.attendees]#get event attendees again
        host = User.get_by_auth_id(event_host)
        host_name = host.name
        host_phone = host.phone
        event_template = jinja_environment.get_template('event_page.html')
        event_template_values = {'event_name': event_name,  'event_date':event_date,
        'event_time':event_time,'event_host':event_host, 'search_done':False, 
        'netid':netid, 'event_id':crawl_id, 'stop_list':stop_list,
        'event_attendees':event_attendees, 'event_hosts':event_hosts,
        'current_user':current_user, 'host_name':host_name, 'host_phone':host_phone}
        self.response.out.write(event_template.render(event_template_values))
        
        
class InviteHandler(BaseHandler):
    def get(self):
        #person_id is the invitee's netid
        invitee_id = self.request.get('person_id')
        invitee = User.get_by_auth_id(invitee_id)
        crawl_id = int(self.request.get('event_id'))
        crawl = Event.get_by_id(crawl_id)

        if not str(crawl_id) in invitee.event_list:
            crawl.attendees.append(invitee_id)
            invitee.event_list += str(crawl_id) +','
            crawl.put()
            invitee.put()

        event_name = crawl.name#string
        event_date = crawl.date#date object
        event_time = crawl.time#time object
        event_hosts = crawl.host_list # host's netid
        #netid is always your own netid
        netid = self.request.get('netid')
        current_user = User.get_by_auth_id(netid).name
        event_attendees = [User.get_by_auth_id(x) for x in crawl.attendees]#get event attendees again
        host = User.get_by_auth_id(event_hosts[0])
        host_phone = host.phone
        host_name = host.name

        event_template = jinja_environment.get_template('event_page.html')
        event_template_values = {'event_name': event_name, 'event_date':event_date,
        'event_time':event_time,'event_hosts':event_hosts, 'search_done':False,
        'netid':netid, 'event_id':crawl_id, 'stop_list':crawl.stop_list,
         'event_attendees':event_attendees, 'current_user':current_user,
         'host_phone':host_phone, 'host_name':host_name}
        self.response.out.write(event_template.render(event_template_values))
        
class NextStopHandler(BaseHandler):
    # put your own credentials here 
    def get(self):
        client = TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)
        crawl_id = int(self.request.get("event_id"))
        netid = self.request.get('netid')
        crawl = Event.get_by_id(crawl_id)
        event_name = crawl.name#string
        event_date = crawl.date#date object
        event_time = crawl.time#time object
        event_hosts = crawl.host_list # list of hosts' netids
        event_attendees = [User.get_by_auth_id(x) for x in crawl.attendees]
        current_user = User.get_by_auth_id(netid).name
        host = User.get_by_auth_id(event_hosts[0])
        host_phone = host.phone
        host_name = host.name

        if not len(crawl.stop_list) == 0:
            numbers = [User.get_by_auth_id(person).phone for person in crawl.attendees]
            stop = crawl.stop_list.pop(0)
            crawl.put()
            stop_location = stop['college_location']
            room_number = stop['room_number']

            for num in numbers:
                client.messages.create(
                    to=num, 
                    from_="+14844517190", 
                    body='%s is moving to: %s %s.' % (event_name,stop_location,room_number)  
                    )

        event_template = jinja_environment.get_template('event_page.html')
        event_template_values = {'event_name': event_name, 'event_date':event_date,
        'event_time':event_time,'event_hosts':event_hosts, 'search_done':False,
        'netid':netid, 'event_id':crawl_id, 'stop_list':crawl.stop_list,
         'event_attendees':event_attendees, 'current_user':current_user,
         'host_phone':host_phone, 'host_name':host_name}
        self.response.out.write(event_template.render(event_template_values))

class VenmoHandler(BaseHandler):
    def get(self):
        event_id= self.request.get('event_id')
        venmo_stop = self.request.get("venmo_stop")
        crawl =Event.get_by_id(event_id)
        stop_list = crawl.stop_list
        stop = stop_list[index]
        stop_host_netid = stop['stop_host']
        stop_host = User.get_by_auth_id(stop_host_netid)
        stop_host_name = stop_host.name
        stop_host_phone = stop_host.phone
        netid = self.request.get('netid')
        
        event_name = crawl.name#string
        event_date = crawl.date#date object
        event_time = crawl.time#time object
        event_hosts = crawl.host_list # list of hosts' netids
        event_attendees = [User.get_by_auth_id(x) for x in crawl.attendees]
        current_user = User.get_by_auth_id(netid).name
        
        template = jinja_environment.get_template('event_page.html')
        event_template_values = {'event_name': event_name,  'event_date':event_date,
        'event_time':event_time,'event_host':event_host, 'search_done':False, 
        'netid':netid, 'event_id':crawl_id, 'stop_list':stop_list, 'host_phone':         stop_host_phone, 'event_attendees':event_attendees,                                   'current_user':current_user, 'host_name':stop_host_name,                             'host_phone':stop_host_phone}
        self.response.out.write(event_template.render(event_template_values))
        
class DeleteStopHandler(BaseHandler):
    def get(self):
        crawl_id = int(self.request.get('event_id'))
        netid = self.request.get('netid')
        crawl = Event.get_by_id(crawl_id)
        stop_index = int(self.request.get('stop_index'))

        crawl.stop_list.pop(stop_index)
        crawl.put()

        # boilerplate data passage below....
        stop_list = crawl.stop_list
        event_name = crawl.name#string
        event_date = crawl.date#date object
        event_time = crawl.time#time object
        event_hosts = crawl.host_list # list of hosts' netids
        event_attendees = [User.get_by_auth_id(x) for x in crawl.attendees]
        current_user = User.get_by_auth_id(netid).name
        # only display the info of the first host
        host = User.get_by_auth_id(event_hosts[0])
        host_phone = host.phone
        host_name = host.name

        event_template = jinja_environment.get_template('event_page.html')
        event_template_values = {'event_name': event_name, 'event_date':event_date,
        'event_time':event_time,'event_hosts':event_hosts, 'search_done':False,
        'netid':netid, 'event_id':crawl_id, 'stop_list':crawl.stop_list,
         'event_attendees':event_attendees, 'current_user':current_user,
         'host_phone':host_phone, 'host_name':host_name}
        self.response.out.write(event_template.render(event_template_values))
        
class DeleteEventHandler(BaseHandler):
    def get(self):
        netid = self.request.get('netid')
        current_user = User.get_by_auth_id(netid).name
        crawl_id = int(self.request.get('event_id'))
        crawl = Event.get_by_id(crawl_id)
        for attendee in crawl.attendees:
            invitee = User.get_by_auth_id(attendee)
            invitee_events = invitee.event_list.split(',')
            invitee_events.remove(crawl_id)
            invitee.event_list = ','.join(invitee_events)
        crawl.key.delete()
        go_to_dash(self, netid, current_user)

class DeleteInviteeHandler(BaseHandler):
    def get(self):
        crawl_id = int(self.request.get('event_id'))
        netid = self.request.get('netid')
        crawl = Event.get_by_id(crawl_id)
        #person_id is the netid of the person to be deleted
        person_id = self.request.get('person_id')
        crawl.attendees.remove(person_id)
        crawl.put()

        #boilerplate data passage below...
        stop_list = crawl.stop_list
        event_name = crawl.name#string
        event_date = crawl.date#date object
        event_time = crawl.time#time object
        event_hosts = crawl.host_list # list of hosts' netids
        event_attendees = [User.get_by_auth_id(x) for x in crawl.attendees]
        current_user = User.get_by_auth_id(netid).name
        # only display the info of the first host
        host = User.get_by_auth_id(event_hosts[0])
        host_phone = host.phone
        host_name = host.name

        event_template = jinja_environment.get_template('event_page.html')
        event_template_values = {'event_name': event_name, 'event_date':event_date,
        'event_time':event_time,'event_hosts':event_hosts, 'search_done':False,
        'netid':netid, 'event_id':crawl_id, 'stop_list':crawl.stop_list,
         'event_attendees':event_attendees, 'current_user':current_user,
         'host_phone':host_phone, 'host_name':host_name}
        self.response.out.write(event_template.render(event_template_values))

config = {
  'webapp2_extras.auth': {
    'user_model': 'model.User',
    'user_attributes': ['name', 'phone', 'email']
  },
  'webapp2_extras.sessions': {
    'secret_key': 'ShEi75i:=8OO7f_i*U`zt`[-;ormI]bKzaOW=n+NS)L&5cNh9UHWEkDQr+{GH@wT'
  }
}

routes = [
    webapp2.Route('/', MainHandler, name='home'),
    webapp2.Route('/signup', SignupHandler),
    webapp2.Route('/login', LoginHandler, name='login'),
    webapp2.Route('/crawl', CrawlEventHandler),
    webapp2.Route('/signout', SignoutHandler),
    webapp2.Route('/search_people', SearchPeopleHandler),
    webapp2.Route('/add_stop', AddStopHandler),
    webapp2.Route('/dash', DashHandler),
    webapp2.Route('/invite_person', InviteHandler),
    webapp2.Route('/next_stop', NextStopHandler),
    webapp2.Route('/move_stop', MoveStopHandler), #this is unused right now
    webapp2.Route('/venmo', VenmoHandler),
    webapp2.Route('/delete_event', DeleteEventHandler),
    webapp2.Route('/delete_stop', DeleteStopHandler),
    webapp2.Route('/delete_invitee', DeleteInviteeHandler)
    #handler=VerificationHandler, name='verification'
]

app = webapp2.WSGIApplication(routes, config = config, debug=True)

# def comp_string(my_search_string)
